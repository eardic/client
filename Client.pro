#-------------------------------------------------
#
# Project created by QtCreator 2012-10-26T14:47:06
#
#-------------------------------------------------

QT       += core
QT      += network
QT       -= gui

TARGET = Client
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Client.cpp

HEADERS += \
    Client.h
