/*

Created BY : EMRE ARDI�
Creation DATE : 26/10/2012

Client interface

*/

#ifndef CLIENT_H
#define CLIENT_H

#include <QtNetwork>
#include <QObject>
#include <QtNetwork/QTcpSocket>

namespace NetworkArdic
{

    class Client : public QObject
    {
        Q_OBJECT
        public:

            Client(QObject * obj = 0,
                   QString add="localhost",
                   quint16 port = 4000);

            // This is the function which sends data to server.
            void SendData(QString data);

            virtual ~Client();

        private slots:

            // This is a function we'll connect to a socket's readyRead()
            // signal, which tells us there's data to be read.
            void ReadData();

            // This function gets called when the socket tells us it's connected.
            void connected();

        private:

            // This is the socket that will let us communitate with the server.
            QTcpSocket *socket;
    };

}

#endif
