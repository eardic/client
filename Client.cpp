/*

Created BY : EMRE ARDI�
Creation DATE : 26/10/2012

Client source file

*/

#include "Client.h"
#include <QHostAddress>
#include <iostream>
using namespace std;

namespace NetworkArdic{

    Client::Client(QObject * obj, QString add, quint16 port) : QObject(obj)
    {
        // Instantiate our socket (but don't actually connect to anything
        // yet until the user clicks the loginButton:
        socket = new QTcpSocket(this);

        // This is how we tell Qt to call our readyRead() and connected()
        // functions when the socket has text ready to be read, and is done
        // connecting to the server (respectively):
        connect(socket, SIGNAL(readyRead()), this, SLOT(ReadData()));
        connect(socket, SIGNAL(connected()), this, SLOT(connected()));

        socket->connectToHost(QHostAddress(add), port);
    }

    Client::~Client(){
        socket->close();
        delete socket;
    }

    // This gets called when the user clicks the sayButton (next to where
    // they type text to send to the chat room):
    void Client::SendData(QString data)
    {
        // Only send the text to the chat server if it's not empty:
        if(!data.isEmpty())
        {
            socket->write(QString(data + "\n").toUtf8());
            socket->waitForBytesWritten();
            socket->flush();
        }
    }

    // This function gets called whenever the chat server has sent us some text:
    void Client::ReadData()
    {
        // We'll loop over every (complete) line of text that the server has sent us:
        while(socket->canReadLine())
        {
            // Here's the line the of text the server sent us (we use UTF-8 so
            // that non-English speakers can chat in their native language)
            QString line = QString::fromUtf8(socket->readLine()).trimmed();
            qDebug() << line;
        }
    }

    // This function gets called when our socket has successfully connected to the
    // server. (see the connect() call in the Client constructor).
    void Client::connected()
    {
        cout << "Client : Server connection has been made (: \n";
    }

}
